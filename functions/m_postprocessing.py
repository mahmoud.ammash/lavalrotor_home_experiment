"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    assert x.shape == y.shape == z.shape, "Input vectors must have the same shape"

    # Calculate the absolute value of the evolution
    vec_accel = np.sqrt(x**2 + y**2 + z**2)

    return vec_accel

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """   
    #try:
        # Linearly interpolate values
    interp_time = np.linspace(min(time), max(time), len(time))
    interp_data = np.interp(interp_time, time, data)

    return interp_time, interp_data
    #except Exception as e:
    #    print(f"Error in interpolation function: {e}")
    #    return None



def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    y = np.mean(x)
    x = x-y
    # Berechnung der FFT
    fft_result = np.fft.fft(x)

    # Berechnung der Frequenzen
    frequencies = np.fft.fftfreq(len(time), time[1] - time[0])

    # Berechnung der Amplitude (Betrag) des FFT-Spektrums
    amplitude = np.abs(fft_result)
    
    # Nur positive Frequenzen betrachten
    positive_freq_mask = frequencies > 0
    amplitude = 2.0 / len(time) * np.abs(fft_result[positive_freq_mask])
    frequencies = frequencies[positive_freq_mask]

    return amplitude, frequencies