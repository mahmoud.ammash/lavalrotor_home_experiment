import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/my_setup_fan.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
acceleration_data = {}

sensor_info = {
    "1ee847be-fddd-6ee4-892a-68c4555b0981": "1ee847be-fddd-6ee4-892a-68c4555b0981"
}

# das innere Dictionary mit leeren Listen 
acceleration_data[sensor_info["1ee847be-fddd-6ee4-892a-68c4555b0981"]] = {
    "acceleration_x": [],
    "acceleration_y": [],
    "acceleration_z": [],
    "timestamp": []
}

# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

i2c = board.I2C()  # Initialize the I2C communication
accelerometer = adafruit_adxl34x.ADXL345(i2c)


start_time = time.time()

while time.time() - start_time < measure_duration_in_s:
    # Read acceleration data
    acceleration_values = accelerometer.acceleration

    # Print acceleration data (you can remove this if not needed)
    print("%f %f %f" % acceleration_values)

    # Store the data in the dictionary using the sensor UUID
    acceleration_data[sensor_info["1ee847be-fddd-6ee4-892a-68c4555b0981"]]["acceleration_x"].append(acceleration_values[0])
    acceleration_data[sensor_info["1ee847be-fddd-6ee4-892a-68c4555b0981"]]["acceleration_y"].append(acceleration_values[1])
    acceleration_data[sensor_info["1ee847be-fddd-6ee4-892a-68c4555b0981"]]["acceleration_z"].append(acceleration_values[2])
    acceleration_data[sensor_info["1ee847be-fddd-6ee4-892a-68c4555b0981"]]["timestamp"].append(time.time())

    # 1-ms-Pause
    time.sleep(0.001)
# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

with h5py.File(path_h5_file, "w") as h5_file:
    raw_data_group = h5_file.create_group("RawData")

    # Create a subgroup for the specific sensor
    sensor_group = raw_data_group.create_group(sensor_info["1ee847be-fddd-6ee4-892a-68c4555b0981"])

    # Store the acceleration data as datasets
    sensor_group.create_dataset("acceleration_x", data=acceleration_data[sensor_info["1ee847be-fddd-6ee4-892a-68c4555b0981"]]["acceleration_x"])
    sensor_group.create_dataset("acceleration_y", data=acceleration_data[sensor_info["1ee847be-fddd-6ee4-892a-68c4555b0981"]]["acceleration_y"])
    sensor_group.create_dataset("acceleration_z", data=acceleration_data[sensor_info["1ee847be-fddd-6ee4-892a-68c4555b0981"]]["acceleration_z"])
    sensor_group.create_dataset("timestamp", data=acceleration_data[sensor_info["1ee847be-fddd-6ee4-892a-68c4555b0981"]]["timestamp"])
   
    # Add attributes to the datasets
    sensor_group["acceleration_x"].attrs["unit"] = "m/s^2"
    sensor_group["acceleration_y"].attrs["unit"] = "m/s^2"
    sensor_group["acceleration_z"].attrs["unit"] = "m/s^2"
    sensor_group["timestamp"].attrs["unit"] = "s"


print(f"Messungen wurden abgeschlossen. Daten wurden in der HDF5-Datei '{path_h5_file}' gespeichert.")

# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
